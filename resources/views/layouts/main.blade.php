<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
</head>

<body>
    <div class="header">
        <ul>
            <li>
                <a href="/">Home</a>
            </li>
        </ul>
    </div>
    <div class="content">
        <h2>@yield('content')</h2>
    </div>
    <div class="footer">
        Laravel 6
    </div>
</body>

</html>