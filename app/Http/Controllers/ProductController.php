<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function index()
    {
        $users = ['A', 'B', 'C'];
        return view('product.index', ['users' => $users]);
    }
}
